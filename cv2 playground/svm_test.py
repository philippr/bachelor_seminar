from sklearn import datasets
from sklearn import svm
import cv2


digits = datasets.load_digits()
clf = svm.SVC(gamma=0.001, C=100.)

clf.fit(digits.data[:-1], digits.target[:-1])
cv2.imshow("",digits.target[-1:])
cv2.waitKey(0)
label = ''.join(clf.predict(digits.data[-1:]))
