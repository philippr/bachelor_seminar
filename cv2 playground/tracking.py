import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

cap = cv.VideoCapture('video.gif')
print(cap)
while(1):
  _, frame = cap.read()
  hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
  
  lower_green = np.array([50,100,50])
  upper_green = np.array([255,140,255])

  mask = cv.inRange(hsv, lower_green, upper_green)

  res = cv.bitwise_and(frame, frame, mask=mask)

  plt.subplot(131)
  plt.imshow(frame)
  plt.subplot(132)
  plt.imshow(mask)
  plt.subplot(133)
  plt.imshow(res)

  plt.show()
