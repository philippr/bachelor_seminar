import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

flags = [i for i in dir(cv) if i.startswith('COLOR_')]
print(flags)



#img = cv.imread('example.jpg',0)
#print(img)
#print(img.shape)
#print(img.size)
#print(img.dtype)

img = np.zeros((512,512,3), np.uint8)
cv.line(img,(250,0),(255,255),(0,0,255),5)
cv.rectangle(img,(10,10),(30,30),(0,255,0),1)
cv.circle(img,(477,63),40,-1) 
font = cv.FONT_HERSHEY_SIMPLEX
cv.putText(img,'Huhu',(10,500), font, 4, 255, 2, cv.LINE_AA)

#plt.imshow(img,'gray'),plt.title('ORIGINAL')
#plt.show()



#cv2.namedWindow('test', cv2.WINDOW_AUTOSIZE) 
#cv2.imshow('image', img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
