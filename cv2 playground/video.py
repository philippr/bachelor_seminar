import cv2
import numpy as np
import time
from skimage.feature import hog

cap = cv2.VideoCapture("test_video.mp4")
fourcc = cv2.VideoWriter_fourcc(*'MP42')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))


ret = True
while ret:
	ret, frame = cap.read()
	frame = cv2.cvtColor(frame, cv2.COLOR_RGB2YUV)
	#gray =  cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
	
	fd, hog_img = hog(frame[:,:,2], orientations=11, pixels_per_cell=(16, 16),
    	                cells_per_block=(2, 2), visualize=True)

	print(frame.shape)
	print(hog_img.shape)
	#print(hog_img[:10,:10])
	#out = cv2.cvtColor(hog_img[:,:], cv2.COLOR_GRAY2BGR)
	cv2.imshow("h", hog_img)


	#cv2.imshow("Hoggy", hog_img)

	#center = frame[(frame.shape[0]//2)//2:1-(frame.shape[0]//2)//2, (frame.shape[1]//2)//2:1-(frame.shape[1]//2)//2]
	#grayCenter = cv2.cvtColor(center, cv2.COLOR_BGR2GRAY)
	#ret, mask = cv2.threshold(grayCenter, 128, 255, cv2.THRESH_BINARY)
	#frame[:center.shape[0], :center.shape[1]]  =  cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
	#out.write(frame)

	#hue sat value

	#lower_red = np.array([140,160,0])
	#upper_red  = np.array([190,255,255])
	#mask = cv2.inRange(hsv, lower_red, upper_red)
	#res = cv2.bitwise_and(frame, frame, mask = mask)


	cv2.imshow("Frame", frame)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

out.release()
cap.release()
cv2.destroyAllWindows()
