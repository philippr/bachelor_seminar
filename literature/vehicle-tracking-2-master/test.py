from scipy.misc import imread
from sklearn.externals import joblib

import random as rand
import numpy as np 
import cv2
import glob
import time

import matplotlib.pyplot as plt

from helpers import convert, show_images
from featuresourcer import FeatureSourcer

sourcer_params = {
  'color_model': 'hls',                # HSL, HSV
  'bounding_box_size': 64,             #
  'number_of_orientations': 12,        # 6 - 12
  'pixels_per_cell': 8,                # 8, 16
  'cells_per_block': 2,                # 1, 2
  'do_transform_sqrt': True
}

start_frame = imread("/home/philipp/studium/bachelor_seminar/src/image_sets/OwnCollection/vehicles/Left/image0010.png")
sourcer = FeatureSourcer(sourcer_params, start_frame)

f = sourcer.features(start_frame)
print("feature shape:", f.shape)

rgb_img, a_img, b_img, c_img = sourcer.visualize()
show_images([rgb_img, a_img, b_img, c_img], per_row = 4, per_col = 1, W = 10, H = 2)
