% --------------------------------------------------------------------------------------------------
% Report template - Seminar "Technology of Autonomous Vehicles"
% Version 1.0, 09-November-2018
% --------------------------------------------------------------------------------------------------
% Prof. Dr. Georg Schildbach
% UNIVERSITAET ZU LUEBECK
% INSTITUTE FOR ELECTRICAL ENGINEERING IN MEDICINE
% --------------------------------------------------------------------------------------------------

\documentclass[11pt,a4paper,oneside]{article}
\usepackage[text={17cm,24.5cm},centering]{geometry}
\addtolength{\topmargin}{-.2in}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

% Fonts --------------------------------------------------------------------------------------------

\usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[cm]{sfmath}
\usepackage{umlaute,eurosym,comment}
\usepackage{amsmath,amssymb,,mathrsfs,amsbsy,bm,upgreek,theorem}
\usepackage{ifpdf,cite}
\usepackage{bibentry}

% Figures and Tables -------------------------------------------------------------------------------

\usepackage{float,epsfig,subfig,wrapfig,xcolor}
\usepackage{wrapfig}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{transparent}
\usepackage{color}
\graphicspath{{figures/}}
\usepackage{url}
% Main Document ------------------------------------------------------------------------------------

\begin{document}



\noindent\textbf{Seminar: Technology of Autonomous Vehicles}\vspace*{0.5cm}\\
\huge
\vspace*{0.4cm}\noindent\textbf{Vehicle Detection}\vspace*{0.5cm}\\
\normalsize
\noindent \vspace*{0.4cm}
Felix Jorcik, Philipp Rothmann


% Inhaltsverzeichnis -------------------------------------------------------------------------------

\pagenumbering{arabic} 
\tableofcontents

\section*{Abstract}
This paper will give you an overview about one approach of vehicle detection for autonomous cars. 
Further, we will show the algorithms Histogram of Oriented Gradients and Support Vector Machines. 
Implementing those and running some tests we made quite decent results. Also we will discuss some
problems and give some outlook regarding possible improvements.


\section{Introduction}\label{Sec:Introdution}

Driving a car requires many abilities. One of the most essential is to see and recognize other
vehicles on the street. 
Autonomous vehicles often have a lot of cameras, which have way better quality than a human eye. But
cameras themselves can not recognize a vehicle. What we need is the brain behind the eyes. 
There are different approaches to make computers recognize objects. In this paper we want to focus on a method that was first
used to detect pedestrians or in more general humans. Nevertheless, with slightly different
configuration this method also does a good job on vehicle objects. This process can be split into two parts. 

First, an image needs to be transformed into more abstract information. This information is called a
feature. In our case, these features need to have a good representation for object-shapes. Cars for
example vary a lot in color, but all have a similar form. To do this we use the
\textit{histogram of oriented gradients (HOG)} feature. It is a smaller, less variant and more
general description of an image and is appropriate for object recognition.

Secondly, we need a machine that tells us if a feature is a vehicle or not. Here we use supervised
machine learning. After training a model with feature vectors of vehicle and non-vehicle images and
also their labels, it returns something called a classifier. This classifier is now able to decide
whether a feature vector describes a vehicle or not. For the second part the
\textit{support vector machine (SVM)} is very useful.
%ONE SENTENCE SVM

Having this, we can now split an image into multiple pieces (sliding window), calculate for each
piece the hog descriptor, throw it into the classifier and mark the piece as a Vehicle if it says yes.


\begin{wrapfigure}{r}{0.4\textwidth}
    \scriptsize
	\begin{center}
		\includegraphics[width=8cm]{./figures/hog.png}
	\end{center}
	\caption{Principle of HOG features.\cite{intelFig}}
	\normalsize
\end{wrapfigure}
\section{Methods and Algorithms}\label{Sec:Methods}
\subsection{Histograms of Oriented Gradients (HOG)}

To extract the feature of an image we use the histogram of oriented
gradients. HOG's extract the gradients orientation and create a histogram of the occurring orientations
for each cell. That works as explained in the following steps: 

Initially, the image is split into blocks and cells. Each cell contains a number of pixels and each block
contains a number of cells. 

Next, the gradient of each pixel is calculated.

\subsubsection{Gradient Calculation}

The gradient of a pixel can be described as the change of intensity between its neighboring pixels for
the x- and aswell for the y-direction.
One way to receive that is to calculate the derivative, or in a discrete sense, the difference
between the two pixels.\\ 
\noindent $I_x(r,c) = I(r,c+1) - I(r,c-1)$ \\
$I_y(r,c) = I(r-1,c) - I(r+1,c)$ \cite{Tomasi2015HistogramsOO} \\

Using convolution, this can be done more efficienctly.
Dalal and Triggs have shown that a  kernel like  $[-1,0,1]$, even though it is so simple,
behaved better compared to other, like a Sobel- or Gaussian-Kernel\cite{dalal2005histograms}. 
These two formulas can be used to compute the magnitude and direction of the gradient:

\noindent Magnitude: $\mu = \sqrt{I_x^2+I_y^2}$  \\
Direction: $\theta =  \frac{180}{\pi} (tan_2^{-1} (I_y, I_x) \mod \pi)$ \cite{Tomasi2015HistogramsOO}

\subsubsection{Orientation Binning}

    A histogram represents the distribution of some variable. In our case we want to know the
    distribution of the gradient orientations in a cell. All cell vectors are placed into evenly spaced
    bins, according to their orientation. The vectors magnitudes accumulate in the bins. The cells
    might be circular or rectangular. The orientation bins range from $0^\circ-180^\circ$ for unsigned
    and from $0^\circ-360^\circ$ for signed vectors\cite{dalal2005histograms}. The number of
    orientation bins is essential for the level of detail of the HOG descriptor.    
    For example, if we consider 9 bins, the vectors from $0^\circ-20^\circ, 20^\circ-40^\circ,
    \dots, 160^\circ-180^\circ$ share the same
    bin. 

\subsubsection{Block Normalization}

    Next, the gradient magnitude is normalized. That is needed to make the descriptor less sensitive to light,
    contrast and other local variations and shows a general better performance. 
    Blocks are grouped from cells by the defined number of cells per block. The total number of
    blocks is calculated in this way:\\
    \indent$\text{n-cells} - \text{cells-per-blocks} = \text{n-blocks} + 1$.\\
    In this way the blocks overlap and the cells contribute to more than one block.  
    Now a normalization formula is applied to each block. Usually one of these methods is taken:
    L1-norm, L1-sqrt, L2-norm or L2-hys. \cite{dalal2005histograms}

\subsubsection{Feature Vector}
    
    A color image usually consists of three dimensions (red, green and blue channel). To compute the
    HOG vector we need either to convert the image into a gray scale format or compute the HOG vector
    for each color channel separately and append the vectors in the end.
    Now the final feature vector contains all the normalized blocks and has a shape of
    n-channels x blocks x
    blocks x cells x cells x n-orientation bins.

\begin{figure}[H]
    \scriptsize
	\begin{center}
		\includegraphics[width=3cm]{./figures/car.png}
		\includegraphics[width=3cm]{./figures/car-hog.png}
		\includegraphics[width=3cm]{./figures/non-car.png}
		\includegraphics[width=3cm]{./figures/non-car-hog.png}
	\end{center}
	\caption{Visualization of the hog feature vector of a vehicle and non-vehicle image\cite{GTI2012}} 
	\normalsize
\end{figure}
\subsection{Support Vector Machine (SVM)}

\begin{wrapfigure}{r}{0.4\textwidth}
    \scriptsize
	\begin{center}
		\includegraphics[width=6cm]{./figures/svm_pseudo.png}
	\end{center}
	\caption{SVM-Pseudocode\cite{svmPseudo}}
	\normalsize
\end{wrapfigure}
    
    In nature there are numerous cases in which a problem is defined precisely and has an exact way
    of calculation which leads to its solution (like the planets orbits). Some problems do not have such
    a mathematical regularity (i.e. handwriting, image recognition). Nevertheless, a human being is
    capable of learning to read and write by examples and exercises. A machine is capable to do the
    same under certain restrictions.
    To give an easy approach to Support Vector Machines, let’s take a cat, which has made several
    bad experiences with a dog so that it learns to avoid confrontations with dogs. If that cat sees a
    dog on the premise of classifying the dog as such she decides based on her experiences how she
    reacts; she escapes. The classification is made by the SVM.
    The Support Vector Machine (SVM) is one of the most important mathematical methods in the
    history of pattern recognition. It is a machine learning algorithm which is capable of supervised
    classification or even regression problems. We will focus on classification.


\begin{wrapfigure}{r}{0.4\textwidth}
    \scriptsize
	\begin{center}
		\includegraphics[width=6cm]{./figures/svm_hyperplane.png}
	\end{center}
	\caption{Hyperplane\cite{svmHyper}}
	\normalsize
\end{wrapfigure}

    \subsubsection{Linear Perceptrons}
    The Support Vector Machine is based on the concept of Linear Perceptrons\cite{minsky69perceptrons}.
    Linear perceptrons work under the assumption that our dataset is linearly separable. The algorithm learns a binary
    classifier whose output works like a threshold function. The learning phase is executed as follows:
   
   A real-valued vector of any dimension is taken and produces a binary output value (\{1,-1\} or
   \{1,0\}) by multiplying the input vector with a weight
    vector w and adding a bias. If the classification is correct nothing is done, if not, the weight vector
    and bias are corrected, so that false negatives are added to the weight vectors  multiplied by a learning rate. False positives are
    subtracted in the same way. 


    On the premise of the datasets linear separability, perceptrons deliver a hyperplane. Depending on
    the dimensionality n of the dataset we get a n-1 -dimensional hyperplane. However, in most cases,
    it will not be the best solution to our problem, since those hyperplanes are not unequivocal.
    Although a small learning rate prevents the algorithm from an endless loop, depending on the
    order of the dataset, while the perceptron is undergoing its learning phase, we get a different final
    state. To find an optimal hyperplane the best-known method (among many others like the least
    squares method ) is the "Maximal Margin Classifier"\cite{boser1992}. Again it just works for
    linearly separable data. The solution is a hyperplane of a maximum distance from the closest data
    points of both classes. This works by "Lagrange Multiplication" to find a global optimum, but here
    we will not explore this further due to complexity.



\subsubsection{Nonlinear classification}

    What if our data is more complex and not linearly separable?
    Again, there are many methods to cope with the issue of data where an initial linear separating
    hyperplane can not be found. The simplest and most occurring way might be to accept that there
    are data points that remain classified wrong, in order to achieve correct classification for most of
    the data. There is also the approach of working with a probability. Intuitively, we are more assured
    to make correct classification if our data point is further away from the hyperplane. For more
    accurate classification those methods will not be sufficient to a nonlinear problem.
    Perceptrons only work with linear data. Therefore, the data has to be linearized.
    This is the so-called "kernel trick" \cite{Burges98atutorial}.We need to enlarge the feature space in
    order to accommodate a non-linear boundary between the classes.
    There are polynomial, radial and many more kernel methods.
    They all have in common that they transform the dataset into a higher dimension. As shown below,
    a 2-dimensional feature space is transformed into 3D (A line is transformed into a real plane).

\begin{figure}[H]
    \scriptsize
	\begin{center}
		\includegraphics[width=14cm]{./figures/svm_kernel.png}
	\end{center}
	\caption{"kernel-trick" \cite{svmKernel}}
	\normalsize
\end{figure}


\section{Implementation}

In the following section we want to elaborate our experimental implementation of a vehicle detector.
We used python and mainly the libraries open-cv2 and sklearn / skimage.

\subsection{HOG-Parameter}

One key element of our test setup is the parameter configuration of the HOG descriptor. The
challenge was to get the right balance of detail and efficiency of the descriptor. 
A larger number of orientation bins and less pixels per cells result in a more detailed descriptor. 
Up to a certain point richer descriptors have a better performance (in a sense of image recognition), but also need 
more memory and computation time. Descriptors with less detail are lighter and faster, but perform
worse at some point.\\
We also experimented with different Color-Spaces:
\\
\begin{table}[H]
\begin{tabular}{|c|c|}
    color space & svm accuracy\\
    \hline
    GRAY &	96\% \\
    RGB & 	97\% \\
    HLS &	98\% \\
    YCrCb & 	99\% 
\end{tabular}
\end{table}


Our final parameters are:
\begin{itemize}
\item Pixels per Cell: 16x16
\item Cells per Block: 2x2
\item Number of Orientation Bins: 12
 \item Block Normalization: L2-Hys
 \item Color-Space: YCrCb  
\end{itemize}

\subsection{SVM-Training}

Our training image dataset consisted of around 9000 vehicle and non-vehicle images. Therefore we used
a combination of the GTI\cite{GTI2012} and the
Kitti\cite{Geiger2012CVPR} database.
The images have a shape of 64x64px which yields in a HOG vector (with the above configuration) of
$3x3x3x2x12$, which is a reasonable size.
Thanks to parallelization of the HOG computation, the training took only a couple of minutes.
Testing the SVM on 20\% of its training data gave us an accuracy of 99\%.

%TODO using standard scaler

\subsection{Sliding Window}

    A simple scenario for vehicle detection would be a camera inside a cars windshield recording video of the street.
    The video footage would probably have a high resolution like 1920x1080px. To now search for
    vehicles in a frame of the video, we need to split it into several windows of different sizes.
    Because the training images we used had a shape of 64x64px we need to resize each window to this
    size. Otherwise, the computed HOG feature vector would not have the same shape as the
    classifier would expect.
    Divergent from our expectations we experienced the most problems at this point. 
    First we thought that the more detection windows we have, the better the score would be. But actually
    only the number of false positives increased.
    As it turned out, the least complex approach worked best and most reliable.
    We cut the upper half of the image to get rid of the sky, which does not 
    contain any vehicles. Then we search in window sizes of 64x64, 128x128 and 512x512 overlapping
    to $\frac{1}{3}$ in the remaining image. 
    Calculating HOG-Feature for each window can be very time consuming. Although that task
    can be easily parallelized, nevertheless the computation time is still too long for a live video.
    We further tried to only search with smaller windows in the upper area and with larger windows
    in the lower area of an image. This reduced the number of windows, but also reduced the total
    reliability. 
    A faster approach is to only compute the HOG feature of the whole frame once. Now instead of
    thinking in pixels we think in blocks of the HOG vector. We take parts of the HOG vector in the shape
    of our training data HOG vector and classify it in the SVM. If it is successful, we can reconstruct the
    pixel coordinates.
    %TODO explain hog window search

\begin{figure}[H]
    \scriptsize
	\begin{center}
		\includegraphics[width=14cm]{./figures/sliding_window.png}
	\end{center}
	\caption{Example of Sliding Windows}
	\normalsize
\end{figure}

\subsection{Heat Map}
    %interaction figure: http://provim.net/wp-content/uploads/2013/01/heat-map.png
    A heat map is an algorithm whose name has its origin from thermal cameras.
    They are used when normal cameras fail if there is too much darkness or a significant
    difference in heat comparing the object of interest and its surrounding.
    Their output is a map of heat.\cite{haarman2015feature}
    To abstract it, heat maps visualize the density of information of
    interest. To give an easy approach, one of the algorithms most
    common use is in website analysis. The users behavior is analyzed in
    a way that one click on an object is counted as one heat point.
    The more often the users interact with an object the more bright it
    appears on the heat map. The algorithm is very uncomplicated, visual
    and an intuitive way to get a better understanding of human input
    behavior. Another very important use can be found in China, where
    human detection/image recognition widely takes place throughout
    many areas \cite{DBLP:journals/corr/LinCWSC15}. Time-variant heat
    maps can help with identifying human group interactions.
    Heat maps can either be made of monochrome grayscale images,
    binary masks or even colormaps.
    Positive classified areas appear as rectangles on the frame.
    The reason why heat maps are used for vehicle detection is to
    compensate the SVM's inaccuracy between frames. Firstly,
    sometimes there will be false positives. These are areas classified as cars where no cars can be
    found. That happens, when feature vectors of areas were similar. The reason therefor lies in the SVM's training, since
    pictures with cars contain non-car elements, too. We are able to falsify these by implementing a
    threshold of overlapping rectangles (as can be seen in the image below). We implemented it so
    that only 3 or more overlapping frames make a true positive.
    Secondly, we can be more assured of the vehicles exact position, since sometimes a correct
    classified area/rectangle only covers half of the actual vehicle. Again, the stability of a set of frames
    gives more accuracy.


\begin{figure}[H]
    \scriptsize
	\begin{center}
		\includegraphics[width=6cm]{./figures/heatmap_car.png}
		\includegraphics[width=6cm]{./figures/heatmap.png}
		\includegraphics[width=6cm]{./figures/heatmap_thresh.png}
	\end{center}
	\caption{Heatmap of a vehicle detection with (intentionally) many false positives}
	\normalsize
\end{figure}

\section{Results}

In the progress of implementing a vehicle detector some problems occurred, which we want to comment
in this section with our solutions.

One thing we optimized almost continuously were the HOG feature parameters. In the beginning, the
representation was pretty bad, but the following tweaks gave us much better results:
\begin{itemize}
    \item Calculating the HOG vector for all three dimensions of an image and appending them to one
	larger HOG descriptor and also choosing the best color space (see above).
    \item Using more pixels per cells to gain a more general representation of the vehicle. Values
	less than 8x8ppc yield in very large HOG vectors and also perform worse, because of a too
	detailed representation.
\end{itemize}

An additional image preprocessing with a gamma correction, which simply computes the square root
element wise, improved the results further. 
\\
For SVM’s implementation, fetching multiple datasets and adjusting it to the posed problem is a very
time consuming and complicated but still rewarding task. It is of importance to have clean and
preferably small data. In a way the SVM is capable of capturing the identity of a thing. Especially in
image recognition the object of interest has to stand out significantly from the background. In
practice, it is not possible to have just the cars without anything else in the pictures and
actually we would want it to contain pieces of the street, since the sliding windows will
contain them too.
\\

Another problem was the SVM performing well on its own training images, but badly on different test
images. We thought that we might have overfitted the classifier. We only used one dataset,
which only had a few different angles, lightning-conditions and surroundings. Adding another dataset
with about 6000 images resulted in more diversity.

As already mentioned in the Sliding Window section, one of our biggest problems were too many false
positives. Our final solution has only a very limited search space in the image. Playing around with
different window sizes, overlapping values and scaling factors took us pretty much time.
In the end, we had quite good results, but some test images wouldn't be detected correctly. So we
decided to make the sliding windows a little bit more sensitive and add the heat map. The heat map
worked very well and only needed some adjustment of the threshold. 
Considering more than one frame in the heat map, can make a positive contribution
to it's principle, since sequential frames can also improve the
classification’s accuracy by implementing a sequential threshold (assuming that a
cars position changes slowly in relation to the FPS).

\\
Another addition we could think of is, to make the feature vector richer. We could add more information from different sources to
the vector. For example we could add a smaller and blurred version of the same image to the vector.
Also, we could add histograms of the color channels to recognize typical colors from cars or the
environment. But if that would actually increase the performance of the vehicle detector is
questionable.
\\

The final vehicle detector performed quite well with certain limitations. Due to the limited search
space, some camera angles will not be possible. Also, very close and very far vehicle will not be
detected. Moreover, we focused on a highway environment. Other situations as in urban traffic might
have bad detection results. 
Our current implementation has a framerate of 2-10 FPS, depending on the number of search windows.
For a live video pipeline, this would be too slow for an autonomous system to respond properly.
Optimizations on the sliding window and an implementation in c++ could maybe speed this up.

\section{Conclusion}
All things considered, we managed to show that the combination of histogram of oriented gradients 
and the support vector machines is able to make a basic vehicle detector. And yet, our 
implementation is far away from practical use. First of all, such a system needs real-time
computation. Next, it should have more than one reliable source of information, like more
sensory systems (distance, 3d-images, car-to-car communication, \dots). 
Further, if the perceived situation in a video changes it might change the
objects composition. In relation to vehicles and traffic that might be the case after sunset. The
images are darker. But the change could also apply to the differences between highways, crossings
and parking lots, since the traffic's "behaviour" is different. Therefore, one should be intrepid to
simply  separate those situations by training an individual SVM for each case and decide what
situation is present to acquire a better and more overall vehicle detection.

\bibliographystyle{IEEEtran}
\bibliography{bibfile}


\end{document}
