\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Methods and Algorithms}{2}
\contentsline {subsection}{\numberline {2.1}Histograms of Oriented Gradients (HOG)}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Gradient Calculation}{2}
\contentsline {subsubsection}{\numberline {2.1.2}Orientation Binning}{2}
\contentsline {subsubsection}{\numberline {2.1.3}Block Normalization}{3}
\contentsline {subsubsection}{\numberline {2.1.4}Feature Vector}{3}
\contentsline {subsection}{\numberline {2.2}Support Vector Machine (SVM)}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Linear Perceptrons}{4}
\contentsline {subsubsection}{\numberline {2.2.2}Nonlinear classification}{4}
\contentsline {section}{\numberline {3}Implementation}{5}
\contentsline {subsection}{\numberline {3.1}HOG-Parameter}{5}
\contentsline {subsection}{\numberline {3.2}SVM-Training}{5}
\contentsline {subsection}{\numberline {3.3}Sliding Window}{6}
\contentsline {subsection}{\numberline {3.4}Heat Map}{6}
\contentsline {section}{\numberline {4}Results}{7}
\contentsline {section}{\numberline {5}Conclusion}{8}
