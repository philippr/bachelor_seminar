\babel@toc {german}{}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Preprocessing}{6}{0}{2}
\beamer@sectionintoc {3}{Histogram of Oriented Gradients (HOG)}{9}{0}{3}
\beamer@sectionintoc {4}{Support Vector Machine (SVM)}{19}{0}{4}
\beamer@sectionintoc {5}{Post Processing}{37}{0}{5}
\beamer@sectionintoc {6}{Results}{44}{0}{6}
