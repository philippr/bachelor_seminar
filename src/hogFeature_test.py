import cv2
import numpy as np
from hogFeature import HogFeature


path = "/home/philipp/studium/bachelor_seminar/src/image_sets/annotated_dataset/test_images/Car_1479498654982120575.jpg_621-678.jpg"
path = "test_images/test5.jpg"
path = "train/vehicles/GTI_Far/image0002.png"
path = "train/non_vehicles/GTI/image1000.png"
img =cv2.imread(path)

params = dict(
	visualize = True,
	feature_vector = False,
)

print(params)
hog = HogFeature(params)
hog.compute(img)

print(hog.getFeatures().shape)
cv2.imshow("hiohg", hog.getImage()[0])
cv2.imshow("hiohg1", hog.getImage()[1])
cv2.imshow("hiohg2", hog.getImage()[2])

out = hog.getImage()[0] + hog.getImage()[1] + hog.getImage()[2]

max_val =  0

for y in range(out.shape[0]):
    for x in range(out.shape[1]):
        if(out[y,x] >= max_val):
            max_val = out[x,y]

for y in range(out.shape[0]):
    for x in range(out.shape[1]):
        out[y,x] *= (255/max_val)

cv2.imwrite("out.png", out)

cv2.waitKey(0)
cv2.destroyAllWindows()

