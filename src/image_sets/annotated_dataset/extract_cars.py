import cv2
import csv

with open('labels.csv', 'r') as file:
	reader = csv.reader(file, delimiter=',')
	reader.__next__()
	for rows in reader:
		xmin = int(rows[0])
		ymin = int(rows[1])
		xmax = int(rows[2])
		ymax = int(rows[3])
		
		filename = rows[4]
		label = rows[5]

		print(filename, xmin, xmax, ymin, ymax)

		img = cv2.imread(filename)
		img_slice = img[ymin:ymax, xmin:xmax]
		#img_slice = img[533:785, 644:905]

		print(cv2.imwrite(f"test_images/{label}_{filename}_{xmin}-{xmax}.jpg", img_slice))