import numpy as np
import pickle
from sklearn.svm import LinearSVC
from sklearn.preprocessing import StandardScaler
from hogFeature import HogFeature
import cv2
import time
from multiprocessing import Pool

def computeHog(window):
	params = dict(
		visualize = True,
		feature_vector = True,
	)

	hog = HogFeature(params)
	frame = window[0][window[1][0] : window[1][1], window[2][0] : window[2][1]]
	frame_scaled = cv2.resize(frame, (window[3], window[3]))
	hog.compute(frame_scaled)
	f = hog.getFeatures()	
	scaled = scaler.transform(f.reshape(1, -1))
	pred = svc.predict(scaled)
	return (window, pred)


def img_pyr(path):
	frame = cv2.pyrDown(cv2.imread(path))
	rects = cv2.pyrDown(cv2.imread(path))

	cv2.imshow("org", frame)
	print(frame.shape)

	ref_size = 64
	overlap = 0.7
	x_start = 0;
	x_stop = frame.shape[1];
	y_start= frame.shape[0]//2;
	y_stop = frame.shape[0];
	x_span = x_stop - x_start
	y_span = y_stop - y_start
	x_n_pixel = np.int(ref_size * (1-overlap))
	y_n_pixel = np.int(ref_size * (1-overlap))
	x_n_overlap = np.int(ref_size * -overlap)
	y_n_overlap = np.int(ref_size * -overlap)
	x_windows = np.int((x_span - x_n_overlap)/x_n_pixel)
	y_windows = np.int((y_span - y_n_overlap)/y_n_pixel)

	windows = [] 

	for y in range(y_windows):
		for x in range(x_windows):
			x_lower = x*x_n_pixel + x_start
			x_upper = x_lower + ref_size
			y_lower = y*y_n_pixel + y_start
			y_upper = y_lower + ref_size

			if(x_upper <= x_stop and y_upper <= y_stop):
				windows.append([frame, (y_lower, y_upper),(x_lower, x_upper), ref_size])

	pool = Pool()
	preds = pool.map(computeHog, windows)
	pool.close() 
	pool.join()

	for p in preds:
		#if(p[1] == 1):
		cv2.rectangle(rects, (p[0][2][1], p[0][1][1]), (p[0][2][0], p[0][1][0]), (255,0,0))
							#cv2.imshow("orig", frame)
							#cv2.imshow("part", frame_part)
							#cv2.imshow("hog", hog.getImage()[0])
							#cv2.waitKey(0)
							#cv2.destroyAllWindows()

	cv2.imshow("", rects)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
		


def hog_pyr(path):
	frame = cv2.pyrDown(cv2.imread(path))
	rects = cv2.pyrDown(cv2.imread(path))



	params = dict(
		visualize = True,
		feature_vector = False,
	)

	hog = HogFeature(params)


	model_length = 3   # length of the hog_descpritor used in the learned model
	stepsize = 2
	scale_start = 2
	scale_stop = 2
	n_scales = 3
	scale = 1
	hits = []
	hog.compute(frame)
	f = hog.getFeatures()
	hog_img = hog.getImage()
	print(f.shape)

	#cv2.imshow("yuv", frame)
	#cv2.imshow("hog0", hog_img[0])
	#cv2.imshow("hog2", hog_img[2])
	#cv2.imshow("hog1", hog_img[1])
	#cv2.waitKey(0)
	#cv2.destroyAllWindows()

	for scale in np.linspace(scale_start, scale_stop, n_scales):
		print(scale)
		print(frame.shape)
		print(f.shape)

		for lower_y in range(0, f.shape[1], stepsize):
			upper_y = lower_y + model_length
			
			if(upper_y <= f.shape[1]):
				for lower_x in range(0, f.shape[2], stepsize):
					upper_x = lower_x + model_length
					
					if(upper_x <= f.shape[2]):
						f_new = f[:,lower_y:upper_y,lower_x:upper_x,:,:,:]
						scaled = scaler.transform(f_new.ravel().reshape(1, -1))
						pred = svc.predict(scaled)
						print(f"{lower_y} : {upper_y}, {lower_x} : {upper_x} -> {pred}")
						img_ly = (lower_y)*  16
						img_lx = (lower_x)*  16
						img_uy = (upper_y+1)* 16
						img_ux = (upper_x+1)* 16
						
						print(f"{img_ly} : {img_uy}, {img_lx} : {img_uy}") 
						#if(pred):
						cv2.rectangle(rects, (np.int(img_lx*scale), np.int(img_ly*scale)), (np.int(img_ux*scale), np.int(img_uy*scale)), (255,0,(100*scale)))
						#out = frame[img_ly:img_uy,img_lx:img_ux,:]
						#if(out.shape == (64,64,3)):
					#		cv2.imwrite(f"train/{time.time()}.png",out)

		print(scale)
		print((frame.shape[1] // scale),frame.shape[0] // scale)
		frame = cv2.resize(frame, (np.int(frame.shape[1] / scale), np.int(frame.shape[0] / scale)))
		hog.compute(frame)
		f = hog.getFeatures()
		hog_img = hog.getImage()
		cv2.imshow("", frame)
		cv2.waitKey(0)
		cv2.destroyAllWindows()
	cv2.imshow("", rects)
	cv2.waitKey(0)
	cv2.destroyAllWindows()







with open("svm.pkl", "rb") as input:
    svc = pickle.load(input)
with open("scaler.pkl", "rb") as input:
    scaler = pickle.load(input)

#path = "train/vehicles/GTI_Far/image0001.png"
path = "test_images/test00.png"
path = "test_images/test1.jpg"

#img_pyr(path)
hog_pyr(path)


















