import numpy as np
import pickle
from sklearn.svm import LinearSVC
from sklearn.preprocessing import StandardScaler
from hogFeature import HogFeature

with open("svm.pkl", "rb") as input:
    svc = pickle.load(input)
with open("scaler.pkl", "rb") as input:
    scaler = pickle.load(input)

test_frame =  cv2.imread("image_sets/OwnCollection/vehicles/Right/image0110.png")
cv2.imshow("test", test_frame)
cv2.waitKey(0)
params = dict(
	visualize = True,
	feature_vector = True,
	multichannel = False
	)
hog = HogFeature(params)

feature, image = hog.compute(test_frame)
scaled = scaler.transform(feature)
feature = feature.reshape(1,-1)
print(feature.shape)
pred = svc.predict(feature)
print(pred)
