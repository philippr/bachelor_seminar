from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import random as rand
from sklearn.svm import LinearSVC

import glob
import numpy as np
from hogFeature import HogFeature
import cv2
import pickle 
from multiprocessing import Pool
import random

def computeHog(path):
	params = dict(
		visualize = True,
		feature_vector = True,
	)

	print(path)

	hog = HogFeature(params)
	img = cv2.cvtColor(cv2.imread(path), cv2.COLOR_RGB2HLS)
	if((img.shape[0] == 64) and (img.shape[1] == 64) and img.shape[2] == 3):
		hog.compute(img)
		return hog.getFeatures().ravel()
	print("WRONG DIM:" + path)
	return


PATH = "OwnCollection/"
PATH = "train/"
print(PATH)

vehicles = glob.glob(PATH + "vehicles/*/*")
print(f"{len(vehicles)} Vehicle Images")

n_vehicles = glob.glob( PATH + "non-vehicles/*/*")
print(f"{len(n_vehicles)} None-Vehicle Images")

try:
	N_IMG = int(input("Enter Number of test-images :"))
except ValueError:
	print("NaN")


#print(f"Generating HOG-Features with this Parameters:")
#for k,v in params.items():
	#print(f"> {k}: {v}")


pool = Pool()
random.shuffle(vehicles)
random.shuffle(n_vehicles)

vehicle_feature = pool.map(computeHog, vehicles[:N_IMG])
n_vehicle_feature =  pool.map(computeHog, n_vehicles[:N_IMG])

pool.close() 
pool.join()


vehicle_feature = np.asarray(vehicle_feature)
n_vehicle_feature = np.asarray(n_vehicle_feature)

print(vehicle_feature.shape)

stacked = np.vstack((vehicle_feature, n_vehicle_feature)).astype(np.float64)

print(f"stacked:\n{stacked.shape}")

scaler = StandardScaler().fit(stacked)

x = scaler.transform(stacked)
y = np.hstack((np.ones(N_IMG ), np.zeros(N_IMG)))

print(f"x:\n {x.shape}")
print(f"y:\n {y.shape}")


x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = rand.randint(1, 100))
svc = LinearSVC(max_iter=10000)
svc.fit(x_train, y_train)
accuracy = svc.score(x_test, y_test)

print(f"accuracy: {accuracy}")

with open("svm.pkl", "wb") as out:
    pickle.dump(svc, out)

with open("scaler.pkl", "wb") as out:
    pickle.dump(scaler, out)
