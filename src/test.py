import cv2
import numpy as np

frame = cv2.imread("test_images/test1.jpg")
cv2.imshow("test", frame)
cv2.waitKey(0)
h = frame.shape[0]
w = frame.shape[1]
print(w, h)

ratio = w / h
w = 64 if (w%64 == 0) else  round(ratio * 64)
h = 64 if (h%64 == 0) else  round(ratio * 64)
print (w, h)

frame = cv2.resize(frame, (h, w), interpolation=cv2.INTER_AREA)
print(frame.shape)
cv2.imshow("64", frame)
cv2.waitKey(0)


