#!/usr/bin/env python3
import argparse
import glob
import numpy as np
import cv2
import pickle
import random
import time
from multiprocessing import Pool
from sklearn.svm import LinearSVC
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from skimage.feature import hog

ORIENTATIONS = 12
PIXEL_PER_CELL = (16,16)
CELLS_PER_BLOCK = (2,2)
BLOCK_NORM = 'L2-Hys'
TRANSF_SQRT = True 
COLOR_SPACE = cv2.COLOR_BGR2YCrCb

def compute_hog(frame):
    frame = cv2.cvtColor(frame, COLOR_SPACE)
    features = [[] for i in range(frame.ndim)]
    for i in range(0,frame.ndim):
        features[i] = hog(frame[:,:,i], ORIENTATIONS, PIXEL_PER_CELL, CELLS_PER_BLOCK, BLOCK_NORM,
                          visualize=False, transform_sqrt=TRANSF_SQRT,
                          feature_vector=False,multichannel=False)
    return np.asarray(features)

def load_hog(img_path):
    frame = cv2.imread(img_path)
    return compute_hog(frame).ravel()

def train_svm(test_img_path, n_img=0):
    vehicle_path = glob.glob(test_img_path + "vehicles/*/*")
    n_vehicle_path = glob.glob(test_img_path + "non_vehicles/*/*")
    if(n_img == 0):
        n_img = len(vehicle_path) if len(vehicle_path) < len(n_vehicle_path)  else len(n_vehicle_path)
    n_img = int(n_img)
    print("Using ", n_img, " training images")
    pool = Pool()
    random.shuffle(vehicle_path)
    random.shuffle(n_vehicle_path)
    vehicles = np.asarray(pool.map(load_hog, vehicle_path[:n_img]))
    n_vehicles = np.asarray(pool.map(load_hog, n_vehicle_path[:n_img]))
    pool.close()
    pool.join()
    features = np.vstack((vehicles, n_vehicles)).astype(np.float64)
    scaler = StandardScaler().fit(features)
    x = scaler.transform(features)
    y = np.hstack((np.ones(n_img), np.zeros(n_img)))
    x_train, x_test, y_train, y_test = train_test_split(x, y,test_size = 0.2, random_state=random.randint(1,100))
    svc = LinearSVC()
    svc.fit(x_train, y_train)
    accuracy = svc.score(x_test, y_test)

    print("Accuracy: ", accuracy)

    with open('svm.pkl', 'wb') as out:
        pickle.dump(svc, out)
    with open('scaler.pkl','wb') as out:
        pickle.dump(scaler, out)

def classify(window):
    frame = cv2.resize(window[1], (64,64))
    feature = compute_hog(frame).ravel()
    feature = scaler.transform(feature.reshape(1,-1))
    pred = svc.predict(feature)
    return (window[0], pred)

def hog2pix(b_x,b_y):

    """
    0      1     2    3
    0-32 16-48 32-64 48-80

    (cpb *  ppc) * n/(block+1) 
    ___________
    |_|_|_|_|_|
    |_|_|_|_|_|
    |_|_|_|_|_|

    64 / (16*2) + 1  = blocks
    (blocks - 1) * 16*2 = 64
    """
     
    


def find_vehicles_hog(img):
    img_half = img # img[img.shape[0]//2:,:,:]
    step_size = 1
    block_size = 3 #hog.shape = [3 x 3 x 3 x 2 x 2 x 12]
    overlap = [0, 2] 
    fac = (block_size - 1) * CELLS_PER_BLOCK[0] * PIXEL_PER_CELL[0]
    print("FUCK: ", fac)
    hog = compute_hog(img_half)

    print(hog.shape)
    rects = []
    

    for y in range(0, hog.shape[1] - (block_size), step_size):
        for x in range(0,hog.shape[2] - (block_size), step_size):
            slice = hog[:,y:y+block_size, x:x+block_size,:,:,:]
            pix_x = int(fac * x)
            pix_y = int(fac * y)
            scaled = scaler.transform(slice.ravel().reshape(1,-1))
            print(slice.shape)
            if not (svc.predict(scaled)):
                print(pix_x, pix_y)
                cv2.imshow("find", img_half[pix_y:pix_y+fac,pix_x:pix_x+fac,:])
                cv2.waitKey(0)
                cv2.destroyAllWindows()
                rects.append(((pix_x,pix_y),(pix_x+fac, pix_y + fac)))
    img_half = cv2.pyrDown(img_half)
    hog = compute_hog(img_half)

    return rects

def find_vehicles(img):
    img_half = img[img.shape[0]//2:,:,:]
    overlap =3
    #window_sizes_x = [img.shape[1]//8,img.shape[1]//4]
    window_sizes_x = [64,128]
    window_sizes_y = window_sizes_x
    windows = []
    for window in window_sizes_x:
        print(window)
        for y in range(0, img_half.shape[0] - window, window // overlap):
            for x in range(0, img_half.shape[1] - window, window // overlap ):
                windows.append([[y,y+window,x,x+window],img_half[y:y+window,x:x+window,:]])
                #cv2.rectangle(img, (x,y+img.shape[0]//2),(x+window,y+window+img.shape[0]//2), (255-window*2,0,0))
    print("Number of Windows: ", len(windows))
    pool = Pool()
    preds = pool.map(classify, windows)
    vehicles = []
    for p in preds:
        if(p[1] == 1):
            rect = [(p[0][2], p[0][0]+img_half.shape[0]), (p[0][3],p[0][1]+img_half.shape[0])]
            vehicles.append(rect)
    return vehicles

def heatmap(frame, vehicles, threshold):
    heat = np.zeros_like(frame)
    for v in vehicles:
        heat[v[0][1]:v[1][1],v[0][0]:v[1][0],:] += 10 
    cv2.imwrite("heatmap.png", heat) 
    ret, heat = cv2.threshold(heat, threshold, 255, cv2.THRESH_BINARY)
    #heat = cv2.bitwise_and(frame, heat)
    return heat

def pipeline(video):
    cap = cv2.VideoCapture(video)
    ret = True
    frame_w = int(cap.get(3))
    frame_h = int(cap.get(4))
    for i in range(6):
        print(cap.get(i))
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out =  cv2.VideoWriter("out.avi", fourcc, cap.get(5), (frame_w, frame_h)) 
    heat = np.zeros((frame_h,frame_w,3))
    while(cap.isOpened):
        ret, frame = cap.read()
        if ret:
            start = time.time()
            rects = find_vehicles(frame)
            print(time.time() - start)
            #for rect in rects:
            #    cv2.rectangle(frame, rect[0], rect[1], (0,255,0))
            heat += heatmap(frame, rects, 10)
            heat //= 5
            frame[:,:,2] = cv2.add(np.asarray(frame[:,:,2], np.float64),
                                   np.asarray(heat[:,:,2], np.float64))
            out.write(frame)
        else:
            break

    cap.release()
    out.release()

descr = """Another Vehicle-Detection
Histogram of oriented Gradients and Support Vector Machine.
Superduper cool!
"""

parser = argparse.ArgumentParser(description=descr)
parser.add_argument('-hog', '--hogImage', help='creates and shows the hog feature / image of the given image path')
parser.add_argument('-t', '--training', help='path to folder with training images containing two subfolder with vehicle/ and non_vehicle/')
parser.add_argument('-n', '--nImages', default=0, help='number of test images to use')
parser.add_argument('-f', '--findVehicles', help='finds vehicles in the given image, requires svm.pkl and scaler.pkl')
parser.add_argument('-heat', '--heatMap', help='-f + heatmap')
parser.add_argument('-v', '--video', help='finds vehicles in given video, requires svm.pkl and scaler.pkl')
args = parser.parse_args()
if(args.hogImage):
    frame = cv2.imread(args.hogImage)
    frame = cv2.cvtColor(frame, COLOR_SPACE)
    features = [[] for i in range(frame.ndim)]
    hog_img = [[] for i in range(frame.ndim)]
    for i in range(0,frame.ndim):
        features[i], hog_img[i] = hog(frame[:,:,i], ORIENTATIONS, PIXEL_PER_CELL, CELLS_PER_BLOCK, BLOCK_NORM,
                          visualize=True, transform_sqrt=TRANSF_SQRT,
                          feature_vector=False,multichannel=False)
    
    for i in range(3):
        cv2.imwrite(f"img{i}.png", frame[:,:,i]);
        max_val = np.amax(hog_img[i])
        out = np.multiply(hog_img[i], 255/max_val)
        cv2.imwrite(f"hog_img{i}.png", out);

if(args.training and args.nImages):
    train_svm(args.training, args.nImages)
elif(args.training):
    train_svm(args.training)

with open("svm.pkl", "rb") as input:
    svc = pickle.load(input)
with open("scaler.pkl", "rb") as input:
    scaler = pickle.load(input)

if(args.findVehicles):
    img = cv2.imread(args.findVehicles)
    for rect in find_vehicles(img):
        cv2.rectangle(img, rect[0], rect[1], (0,255,0))
    cv2.imshow("vehicle", img)
    cv2.waitKey(0)
    cv2.imwrite("out.png", img)
    cv2.destroyAllWindows()
    cv2.imwrite("rects.png", img)
if(args.heatMap):
    img = cv2.imread(args.heatMap)
    cv2.imwrite("heatmap_thresh.png", heatmap(img, find_vehicles(img), 40))
if(args.video):
    pipeline(args.video)
